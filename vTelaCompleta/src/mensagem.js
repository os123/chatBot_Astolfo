class Mensagem{
  constructor() {
    this._autor = "Sem autor";
    this._mensagem = "Sem Mensagem";
    this._status = false;
    this._resposta = '';
  }

  get autor(){
    return this._autor;
  }

  set autor(__autor){
    this._autor =__autor;

  }

  get mensagem(){
    return this._mensagem;
  }

  set mensagem(__mensagem){
    this._mensagem = __mensagem;

  }

  get status(){
    return this._status;
  }

  set status(__status){
    this._status = __status;

  }

  get resposta(){
    return this._resposta;
  }

  set resposta(__resposta){
    this._resposta = __resposta;

  }    
  

  mensagemJson(){
    let mensagemObject ={
      autor: this._autor,
      mensagem: this._mensagem
    };  
    return mensagemObject;
  }

}
module.exports = Mensagem;