var socket = io('http://localhost:3000');

socket.on('receiveMensagem',function (mensagem){
	$('<li class="sent"><img id="profile-img" src="/images/avatar.png" class="online" ><p>' + mensagem.mensagem + '</p></li>').appendTo($('.messages ul'));
	$('.message-input input').val(null);
	$('.contact.active .preview').html('<span>You: </span>' + mensagem.mensagem);
	$(".messages").animate({ scrollTop: $(document).height() }, "fast");
});

socket.on('removeAllmensagem',function (){
	console.log('AquiPereira');
	var node = document.getElementById("messages ").getElementsByTagName("LI");
	console.log(node);
	if (node.parentNode) {
	  node.parentNode.removeChild(node);
	}

});

$(".messages").animate({ scrollTop: $(document).height() }, "fast");

function renderMensagem(message) {
	if($.trim(message) == '') {
		return false;
	}
	$('<li class="replies"><img id="profile-img" src="/images/user.jpg" class="online" ><p>' + message + '</p></li>').appendTo($('.messages ul'));
	$('.message-input input').val(null);
	$('.contact.active .preview').html('<span>You: </span>' + message);
	$(".messages").animate({ scrollTop: $(document).height() }, "fast");

};

function sendMensagens() {
	let mensagemObject = $(".message-input input").val();
	renderMensagem(mensagemObject);
	socket.emit('sendMensagem', mensagemObject);

}
document.getElementById("sendButtonMenssage").onclick = (() => sendMensagens());

//By clicking enter, you will receive a message
$(window).on('keydown', function(e) {
	if (e.which == 13) {
		sendMensagens();
		return false;
	}
});
