var socket = io('http://localhost:3000');
$('#chat').submit(function(event){
	event.preventDefault();
	let mensagemObject = $('input[name=mensagem]').val();
	renderMensagem(mensagemObject);
	socket.emit('sendMensagem', mensagemObject);


});

function renderMensagem(mensagem){
	$('.msg_container_base').append(''+
		'<div class="row msg_container base_sent">'+
			'<div class="col-md-10 col-xs-10">'+
	            '<div class="mensagens msg_sent">'+
	                '<p><strong>Você:</strong>'+mensagem+'</p>'+
	            '</div>'+
	        '</div>'+
			'<div class="col-md-2 col-xs-2 avatar">'+
			   '<img src="/images/user.jpg" class=" img-responsive ">'+
	        '</div>'+

	    '</div>');

}

socket.on('receiveMensagem',function (mensagem){
	$('.msg_container_base').append(''+
		'<div class="row msg_container base_receive">'+
			'<div class="col-md-2 col-xs-2 avatar">'+
			   '<img src="/images/avatar.png" class=" img-responsive ">'+
	        '</div>'+
	        '<div class="col-md-10 col-xs-10">'+
	            '<div class="mensagens msg_receive">'+
	                '<p><strong>'+mensagem.autor+':</strong>'+mensagem.mensagem+'</p>'+
	            '</div>'+
	        '</div>'+
	    '</div>');
});

socket.on('removeAllmensagem',function (){
	console.log('AquiPereira');
	var node = document.getElementById("conteudo");
	if (node.parentNode) {
	  node.parentNode.removeChild(node);
	}

});


/*function rendermeensagem(meensagemResultado){
	$('.message123').append(''+
		'<div class="col-md-2 col-xs-2 avatar">'+
		   '<img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">'+
        '</div>'+
        '<div class="col-md-10 col-xs-10">'+
            '<div class="meensagens msg_receive">'+
                '<p><strong>Você:</strong>'+message.message+'</p>'+
            '</div>'+
        '</div>');

}*/
