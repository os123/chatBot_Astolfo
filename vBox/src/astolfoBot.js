#!/usr/bin/env node

// JavaScript RiveScript benchmark for A.L.I.C.E.

const RiveScript = require("rivescript");
const path = require('path');
const bot = new RiveScript({utf8: true});
const Mensagem = require('./mensagem.js');

class Astolfo {
  constructor() {
		//bot.loadDirectory(path.join(__dirname, './alice/'));
    bot.loadDirectory(path.join(__dirname, '../../Mind/'));
    bot.sortReplies();
    this._nome = 'Astolfo';
    this.mensagemGerador =
      (texto) =>
      {
        let resposta= new Mensagem();
        resposta.autor = this._nome;
        resposta.mensagem = texto;
        resposta.status = false;
        return resposta;
      }
  }

  mensagem(line) {
  	//line = line.mensagem.trim();
  	bot.sortReplies();
    let resposta = bot.reply("User", line);
    return this.mensagemGerador(resposta);
  }

  get nome(){
    return this._nome;
  }

  set nome(__nome){
    this._nome = __nome;
  }

  static nm(){
    this._nome;
  }


}


module.exports = Astolfo;
