const express = require('express');
const path = require('path');
const favicon = require('static-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const routes = require('./../routes/index');
const users = require('./../routes/users');
const app = express();

//inicializar o servido onlhar o bin/www https://stackoverflow.com/questions/24609991/using-socket-io-in-express-4-and-express-generators-bin-www
const server = require('http').Server(app);
var io = require('socket.io')();

//chamada chat bot
const AstolfoBot = require("./astolfoBot.js");
//leitor de rive tipo de arquivo AIML simplificado
const RiveScript = require("rivescript");
const bot = new AstolfoBot();
const Mensagem = require('./mensagem.js');

app.io = io;
// view engine setup
app.set('views', path.join(__dirname, './../views'));
app.set('view engine', 'ejs');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, './../public')));

app.use('/', routes);
app.use('/users', users);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    console.log(err+"-00");
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

//importanto socket.io
function resposta(socket, mensagem) {
    if(typeof mensagem  === 'object'){
        setTimeout(function(){
            socket.emit('receiveMensagem',mensagem.mensagemJson());
        },0);
    }

}

function say(line) {
    let resposta= new Mensagem();
    resposta.autor = bot.nome;
    resposta.mensagem = line;
    resposta.status = false;
    return resposta;
}

io.on('connection',socket =>{
    console.log(`Socket conectado : ${socket.id}`);
    socket.on('sendMensagem', line =>{
        resposta(socket, bot.mensagem(line));
    });
    socket.on("disconnect", function(){
        console.log("Disconnect");

    });
});

 module.exports = app;
