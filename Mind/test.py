#!/usr/bin/env python
# -*- coding: utf-8 -*-
# lendo as linhas do arquivo
pasta = './salutations.rive'
with open(pasta, 'r') as arquivo:
    linhas = arquivo.readlines() #cada linha é um elemento da lista linhas

num_elementos_lista = len(linhas)
x = 0
while(x < num_elementos_lista):
    linhas[x] = linhas[x].lower()
    x+=1

for line in linhas:
    if line[0] == "+":
        print(line)
# escrevendo de novo
with open(pasta, 'w') as arquivo:
    arquivo.writelines(linhas)
