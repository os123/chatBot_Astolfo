 Astolfo BOT
==========
Conceitos básicos
----------------------

## JavaScript 
>	JavaScript é uma linguagem de programação que permite a você implementar itens 
complexos em páginas web — toda vez que uma página da web faz mais do que simplesmente  mostrar a você informação estática — mostrando conteúdo que se atualiza em um intervalo de  tempo, mapas interativos ou gráficos 2D/3D animados, etc. — você pode apostar que o 
JavaScript provavelmente está envolvido.
>
> Referencia: https://developer.mozilla.org/pt-BR/docs/Learn/JavaScript/First_steps/O_que_e_JavaScript;
>
>### NodeJS
>Node.js é uma plataforma para desenvolvimento de aplicações server-side baseadas em 	rede utilizando JavaScript e o V8 JavaScript Engine, ou seja, com Node.js podemos criar 	uma variedade de aplicações Web utilizando apenas código em JavaScript.
>
>Referencia: https://tableless.com.br/o-que-nodejs-primeiros-passos-com-node-js/;

>#### Guia de Instalação NodeJS
>
>#### Linux
>
>##### Atualizar o sistema:
>
>###### $sudo apt-get update

remoção:
>###### $sudo apt-get remove nodejs
>###### $sudo apt-get autoremove

Opção 1:			
>###### $sudo apt-get install nodejs	
>###### $sudo apt-get install npm		
opção 2 (Instalação via ppa): 
*instale o curl caso não esteja instalado “$ sudo apt install curl”
>###### $cd ~
>###### $curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
>###### $sudo bash nodesource_setup.sh
>###### $sudo apt-get install nodejs
>###### $sudo apt-get install build-essential
//Aṕos executar o comando “nodejs -v” verifique se a versão do programa instalado e <= “v8.12.0”
>###### $nodejs -v
//Aṕos executar o comando “npm -v” verifique se a versão do programa instalado e <= “6.4.1”
>###### $npm -v
//caso uma das opções não de certo tente outra  após executar a remoção
referencia:https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04#removing-nodejs
										
>####Como Usar NPM
	NPM (Node Package Manager) é só um pacote de gerenciamento de módulos de códigos JS para instalar junto ao NodeJs e poder usar nas suas aplicações, ou até mesmo suas aplicações que precisam ser incluídas junto ao Node.

Comandos:
>###### $npm install npm@latest -g
|  -- | instalar| o programa@vesão| complemento
ex: npm i cli-color@0.1.6 
- g  = O complemento “-g” instalado de formar, que e ascecivel a todos os programas do computador.
- dev = dependencia usanda no desenvolvimento

Para entender melhor entre em :
https://medium.com/beginners-guide-to-mobile-web-development/introduction-to-npm-and-basic-npm-commands-18aa16f69f6b

referencia:https://pt.stackoverflow.com/questions/157034/o-que-%C3%A9-o-npm-e-o-node
			

>##Express
	Express é um popular framework web estruturado, escrito em JavaScript que roda sobre o ambiente node.js em tempo de execução. Este módulo explica alguns dos principais benefícios deste framework, como configurar o seu ambiente de desenvolvimento e como executar tarefas comuns de desenvolvimento e implantação da web.
	
	A estrutura básica do projeto foi criada utilizando a ferramenta  express-generator  
com o comando express-generator –-ejs. Foi  gerada a estrutura basica abaixo. 

	
    • “public” contem os arquivos do front end do site que vão ser acessador ou execultados.
    • “routes/index.js” rotas para a chamada das paginas.
    • “views” contem as  páginas html/ejs.
    • 		
		itens acresentados:
    • “src” contem os arquivos de back end e banco de dados.
    • “src/Mind/” arquivos de banco de dados.
    • “src/app.js” primeiro arquivo a ser executado inicialmente/ núcleo do programa .  
    • “src/astolfoBot.js” arquivos de banco de dados.
    • “src/mensagem.js/” arquivos de banco de dados.
>##GIT
>	GIT é um sistema de controle de versões distribuído, usado principalmente no desenvolvimento de software, mas pode ser usado para registrar o histórico de edições de qualquer tipo de arquivo. 
>
>###Guia De insatalação
>###### $ sudo apt-get install git-all


>#Explicação do projeto
>Um chatbot através de tecnologias Web, nomeado Astolfo, para auxiliar os estudantes dos cursos de computação a aprenderem o importante tópico de Requisitos de Software.
>##Como iniciar ao software
> Dentro do arquivo vBox ou vTelaCompleta execute o comando
   >##$npm start  - "exulta o programa"
   >##$npm run mon - "exulta o programa com a ferramenta nodemon"
>##Caso de teste
entrada: quem eu sou?
 saída possíveis : 
>•você é um ótimo ouvinte
>•você é uma pessoa realmente forte
>•seu trabalho naquele projeto foi incrível
>•você me inspira
>•você tem um coração lindo
>
##endereço html: https://localhost:3000
>
##Diferença das telas
>###vBox = tela de mensagem em formato de box
>###vTelaCompleta = tela de mensagem ocupa a tela inteira